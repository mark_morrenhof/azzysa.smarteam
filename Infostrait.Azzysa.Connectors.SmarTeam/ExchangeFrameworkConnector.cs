﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Infostrait.Azzysa.EF.Interfaces;
using Infostrait.Azzysa.Interfaces;
using Newtonsoft.Json;

namespace Infostrait.Azzysa.Connectors.SmarTeam
{
    public class ExchangeFrameworkConnector : IExchangeFrameworkProvider
    {

        // Private member(s)
        private readonly IRuntime _runtime;
        private readonly IConnectionLock _connectionLock;

        /// <summary>
        /// Constructs a new Exchange Framework
        /// </summary>
        /// <param name="runtime">Initialized <see cref="IRuntime"/> object</param>
        /// <param name="connectionLock"><see cref="IConnectionLock"/> implementation that holds the appropriate CookieContainer required to establish a connection</param>
        public ExchangeFrameworkConnector(IRuntime runtime, IConnectionLock connectionLock)
        {
            // Default constructor
            _runtime = runtime;
            _connectionLock = connectionLock;
        }

        public IDataPackageBase ProcessPackage(IDataPackageBase package, string configurationName)
        {
            // Convert package to JSON string
            var converter = new Newtonsoft.Json.JsonSerializer();
            var sb = new StringBuilder();
            var writer = new JsonTextWriter(new StringWriter(sb));
            converter.Serialize(writer, package);
            var jsonData = Encoding.UTF8.GetBytes(sb.ToString());
            
            // Initialize WebClient required to invoke Web Api
            var client = new ExtendedWebClient()
            {
                Headers =
                    {
                        [HttpRequestHeader.Accept] = "application/json",
                        [HttpRequestHeader.ContentType] = "application/json"
                    },
                TimeOut = TimeSpan.FromMinutes(20)
            };

            // Invoke Web Api
            try
            {
                // Perform the actual invoke
                var result =
                    Encoding.UTF8.GetString(client.UploadData(_runtime.ServicesUrl + "/ExchangeFramework/Process", "POST",
                        jsonData));

                // Succed, get the returned IDataPackageBase holding the process results
                var reader = new JsonTextReader(new StringReader(result));
                var returnPackage = (IDataPackageBase) converter.Deserialize(reader, package.GetType());

                // Return package has been read into an in-memory object, return to caller
                return returnPackage;
            }
            catch (WebException ex)
            {
                var response = ex.Response;
                if (response != null)
                {
                    var responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var length = responseStream.Length;
                        var errData = new byte[length];
                        responseStream.Read(errData, 0, (int) length);
                        var errorText =
                            Encoding.UTF8.GetString(errData)
                                .Replace("\\r", Environment.NewLine)
                                .Replace("\\n", Environment.NewLine)
                                .Replace("\\t", "     ");

                        throw new ApplicationException(errorText);
                    }
                    else
                    {
                        throw;
                    }
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                client.Dispose();
                sb.Clear();
            }
        }
    }
}