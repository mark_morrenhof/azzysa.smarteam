﻿using System;
using System.Net;

namespace Infostrait.Azzysa.Connectors.SmarTeam
{
    public class ExtendedWebClient : WebClient
    {

        public TimeSpan TimeOut { get; set; }

        public ExtendedWebClient() : base()
        {
            TimeOut = TimeSpan.FromMinutes(2);
        }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var w = base.GetWebRequest(uri);
            if (w != null)
            {
                w.Timeout = TimeOut.Milliseconds > 0 ? TimeOut.Milliseconds : System.Threading.Timeout.Infinite;
            }
            return w;
        }
    }
}