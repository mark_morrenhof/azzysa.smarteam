﻿using Infostrait.Azzysa.Connections.SmarTeam;
using Infostrait.Azzysa.Connectors.SmarTeam;
using Infostrait.Azzysa.Interfaces;
using Infostrait.Azzysa.Providers.SmarTeam;

namespace Infostrait.Azzysa.Runtime.SmarTeam
{
    public class SmarTeamRuntime : PoolRuntime
    {
        public SmarTeamRuntime(string connectionString, string serverName, string siteName, string rootPath) : 
            base(new SettingsProvider(), connectionString, serverName, siteName, rootPath)
        {
        }

        /// <summary>
        /// Returns the <see cref="IExchangeFrameworkProvider"/> implementation that is applicable to the IRuntime implementation
        /// </summary>
        /// <param name="connectionLock"><see cref="IConnectionLock"/> implementation that holds the appropriate CookieContainer required to establish a connection</param>
        /// <returns></returns>
        public override IExchangeFrameworkProvider GetExchangeFrameworkProvider(IConnectionLock connectionLock)
        {
            return new ExchangeFrameworkConnector(this, connectionLock);
        }

        /// <summary>
        /// Clean up, connection pool and any other private member
        /// </summary>
        public override void Dispose()
        {
            // Dispose connection pool
            ConnectionPool?.Dispose();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SmarTeamConnectionPool"/> object
        /// </summary>
        /// <returns></returns>
        protected override IConnectionPool InitializeConnectionPool()
        {
            // Return new instance of the SmarTeamConnectionPool
            return new SmarTeamConnectionPool();
        }
    }
}