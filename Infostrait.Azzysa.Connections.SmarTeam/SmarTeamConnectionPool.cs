﻿using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connections.SmarTeam
{
    public class SmarTeamConnectionPool : IConnectionPool
    {
        public void Dispose()
        {
            // Nothing to dispose
        }

        /// <summary>
        /// For now, returns always one since a dummy object will be generated on each pool object request (Connection Pooling is done server side)
        /// </summary>
        /// <returns></returns>
        public int GetPoolCount()
        {
            return 1;
        }

        /// <summary>
        /// For now, returns always one since a dummy object will be generated on each pool object request (Connection Pooling is done server side)
        /// </summary>
        /// <param name="isAvailable">Parameter not used</param>
        /// <returns></returns>
        public int GetAvailableCount(bool isAvailable)
        {
            return 1;
        }

        /// <summary>
        /// Returns an SmarTeamConnection pool that is available from the internal pool, or create a new one if not available and lock it exclusively to the caller
        /// </summary>
        /// <param name="servicesUrl">Base url to web services, login service URL is constructed based on this url</param>
        /// <param name="userName">User name to connect with (is checked after getting an existing connection from the pool)</param>
        /// <param name="password">Password for the specified user</param>
        /// <param name="vault">Name of the vault to specify on the login procedure</param>
        /// <returns></returns>
        public IConnectionLock GetPoolObject(string servicesUrl, string userName, string password, string vault)
        {
            // Create dummy pool object, and 'lock' it
            var poolObject = new SmarTeamConnection();
            return poolObject.Lock(this);
        }
    }
}