﻿using System;
using System.Net;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connections.SmarTeam
{

    /// <summary>
    /// Dummy IConnection implementation, connection pooling is done at Web Api level
    /// </summary>
    public class SmarTeamConnection : IConnection
    {

        // Private members
        private CookieContainer _cookieContainer;
        internal readonly string SessionId;

        public SmarTeamConnection()
        {
            // Default constructor
            _cookieContainer = new CookieContainer();
            SessionId = Guid.NewGuid().ToString();
        }

        public void Dispose()
        {
            _cookieContainer = null;
        }

        public CookieContainer GetCookieContainer()
        {
            return _cookieContainer;
        }

        /// <summary>
        /// Locks this connection
        /// </summary>
        /// <returns></returns>
        internal SmarTeamConnectionLock Lock(SmarTeamConnectionPool connectionPool)
        {
            var lockObject = new SmarTeamConnectionLock {Connection = this, ConnectionPool = connectionPool};
            return lockObject;
        }

        /// <summary>
        /// Not implemented (yet) for SmarTeam based connections
        /// </summary>
        /// <returns></returns>
        public WebClient GetWebClient()
        {
            return null;
        }

    }
}