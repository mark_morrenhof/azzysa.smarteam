﻿using System.Net;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connections.SmarTeam
{

    /// <summary>
    /// IConnectionLock that is returned from <see cref="SmarTeamConnectionPool.GetPoolObject(string,string,string,string)" />
    /// </summary>
    public class SmarTeamConnectionLock : IConnectionLock
    {

        internal SmarTeamConnection Connection;
        internal SmarTeamConnectionPool ConnectionPool;

        public void Dispose()
        {
            Connection.Dispose();
            Connection = null;
            ConnectionPool = null;
        }

        public IConnection GetConnection()
        {
            return Connection;
        }

        public CookieContainer GetCookieContainer()
        {
            return Connection.GetCookieContainer();
        }

        public string GetSessionId()
        {
            return Connection.SessionId;
        }

        public void Unlock()
        {
            // Dummy - not implemented
        }

        public bool DisposeConnectionOnDispose { get; set; }
    }
}
