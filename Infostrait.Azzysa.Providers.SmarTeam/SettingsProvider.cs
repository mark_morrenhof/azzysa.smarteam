﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Providers.SmarTeam
{

    /// <summary>
    /// <see cref="ISettingsProvider"/> implementation that uses SmarTeam as the settings store
    /// </summary>
    public class SettingsProvider : SettingsProviderBase
    {

        /// <summary>
        /// Default constructor
        /// </summary>
        public SettingsProvider()
        {
            // Default constructor
        }

        private HttpClient _serviceClient;

        private HttpClient GetServiceClient(string serviceUri)
        {
            if (_serviceClient != null) return _serviceClient;
            _serviceClient = new HttpClient {BaseAddress = new Uri(serviceUri) };
            _serviceClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return _serviceClient;
        }

        /// <summary>
        /// Reads all settings from ENOVIA
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <returns></returns>
        public override ISettingsDictionary ReadSettings(IRuntime runtime, string connectionString)
        {
            // Get environmental settings
            var siteName = runtime.SiteName;
            var serverName = runtime.ServerName;

            // Connect to web api and return the settings dictionairy
            var client = GetServiceClient(string.Format(connectionString + "?siteName={0}&serverName={1}", siteName, serverName));
            var response = client.PostAsync(string.Empty, null).Result;

            // Check invocation result
            if (response.IsSuccessStatusCode)
            {
                // Succed, get the ISettingsDictionary
                return response.Content.ReadAsAsync<SettingsDictionary>().Result;
            }
            else
            {
                // Failed invocating the Web Api
                throw new ApplicationException(response.ReasonPhrase);
            }
        }

        /// <summary>
        /// Saves all settings in SmarTeam
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="settings">Settings to write</param>
        /// <remarks>Not implemented, no exception is thrown</remarks>
        public override void SaveSettings(IRuntime runtime, string connectionString, ISettingsDictionary settings)
        {
            // For now, do not save
        }
    }
}